

## 项目介绍
本项目是基于ulthon_admin实现的统一用户授权系统。

# 开发依赖

## 基本环境

只需要最基础的PHP开发环境即可。

- PHP8.0（正确设置PATH环境变量）
- composer
- Mysql5.7+（开发必备）

开发环境中，并不必须安装nginx、apache、ftp等软件，可以直接通过内置服务器进行开发。

> 实际上，如果你使用sqlite开发，连mysql都不想要安装，但是sqlite并不能很好地调整数据表和列，所以一般使用mysql等常规数据库。

## SASS

框架中部分底层组件使用了SASS特性，但一般不需要关心，如果使用vscode，可参考以下内容：

```
名称: Live Sass Compiler
ID: glenn2223.live-sass
说明: Compile Sass or Scss to CSS at realtime.
版本: 5.5.1
发布者: Glenn Marks
VS Marketplace 链接: https://marketplace.visualstudio.com/items?itemName=glenn2223.live-sass
```

## 配置

vscode中liveSassCompiler的配置:

```json
{
    "liveSassCompile.settings.includeItems": [
        "/public/static/common/css/theme/*.scss",
        "/public/static/plugs/lay-module/tableData/tableData.scss",
        "/public/static/plugs/lay-module/tagInput/tagInput.scss",
        "/public/static/plugs/lay-module/propertyInput/propertyInput.scss"
    ]
}
```

# docker

## 打包镜像

下面给出的命令指定了标签名，可自行替换。
使用以下命令打包时，相同的名称不会覆盖，名称会被新的镜像占用，因此用第二行以时间为版本名的方式，调试起来更方面。

```
docker build -t ulthon/ulthon_admin:v1 . 
docker build -t ulthon/ulthon_admin:202404071454 . 
```

## 运行镜像


下面的命令中为容器指定了名字，可自行替换。
相同名称不能重复运行，所以指定名字是个好习惯，否则docker会自动起名。


```
docker run -d \
--restart=always \
-p 88:80 \
-v /data/ulthon_admin/runtime:/var/www/html/runtime \
-v /data/ulthon_admin/storage:/var/www/html/public/storage \
-v /data/ulthon_admin/build:/var/www/html/public/build \
-v /data/ulthon_admin/safe_storage:/var/www/html/storage \
--name ulthon_admin ulthon/ulthon_admin:202404071454 \
server
```

- 端口：需要暴露80端口，冒号左侧可自定义，修改为本机可用的一个端口。
- runtime：需要映射runtime目录，冒号左侧可自定义，修改为指定的目录，推荐设置，因为缓存、日志等文件都保存在这个目录下，不指定的话，重启丢失。
- public/storage：框架默认的上传文件存储在public/storage下，因此必须映射该目录，如果系统不需要上传，则可以不设置。
- public/build：框架默认的生成文件（比如海报、二维码）存储在public/build下，推荐映射，否则重启后丢失。
- storage：框架设计了一个安全存储的位置，相对于storage在public下，storage在项目根目录下，因此无法直接请求到，相对安全，此时只能通过相关支持认证的控制器访问，因此更安全，可以用于存储身份证等敏感信息，但此机制大多数项目用不到，如果用到了必须要设置。
- 后端运行，`-d`参数可以让docker在后端运行。

如果有其他指定的目录，可在Dockerfile中自行添加，在命令中映射。

> 如果不映射目录，镜像不会出错，但重启后丢失。


## 基本用法

内置了docker打包命令，打包出的镜像支持两种调用方法：

- server或空
- think 

有两个基本的用法，运行镜像后，如果在最后增加任何参数或者增加server，会启用镜像中的php-fpm和nginx，使用`-d`后自动运行到后台，相当于框架一键运行。

如果以think开始传参，则是调用think命令，比如`docker run xxxxx think admin:version`，是调用内置命令。

> 实际上可以不以think开始，也可以是其他路径的php文件，但这里不过多讨论

## 其他常用命令

```bash
# 一键删除停止的容器
docker rm $(docker ps -a -q)
# 导出镜像
docker save e950efa97445 -o ulthon_admin.tar  ulthon/ulthon_admin:202404071454
# 导入镜像
docker load -i ulthon_admin.tar
# 删除指定名称的镜像
docker images | grep ulthon_admin | awk '{print $3}' | xargs docker rmi
# 停止指定名称的容器
docker stop ulthon_admin
# 删除指定名称的容器
docker remove ulthon_admin
# 开启交互终端
docker exec -it ulthon_admin /bin/bash
# 删除没有运行的镜像
docker image prune -a
# 移除卷
docker volume prune
# 移除所有卷
docker system prune
```