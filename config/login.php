<?php

use app\common\service\LoginType\WechatOfficialAccountWebviewService;

$config = [];

$config['login_type'] = [
    WechatOfficialAccountWebviewService::TYPE_NAME=>WechatOfficialAccountWebviewService::class,
];

return $config;
