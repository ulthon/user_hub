<?php

use think\facade\App;

// !注意，必须要引入该文件
include App::getRootPath() . '/extend/base/helper.php';


function unparse_url($parsed_url)
{
    $scheme = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user = isset($parsed_url['user']) ? $parsed_url['user'] : '';
    $pass = isset($parsed_url['pass']) ? ':' . $parsed_url['pass'] : '';
    $pass = ($user || $pass) ? "$pass@" : '';
    $path = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    $query = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
    $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';

    return "$scheme$user$pass$host$port$path$query$fragment";
}

// 获取sign
function get_sign($secret, $data)
{
    if (!isset($data['timestamp']) || !$data['timestamp']) {
        $data['timestamp'] = time();
    }
    // 对数组的值按key排序
    ksort($data);
    // 生成url的形式
    $params = http_build_query($data);
    // 生成sign
    $sign = md5($params . $secret);

    return $sign;
}

/**
 * 后台验证sign是否合法.
 * @param  [type] $secret [description]
 * @param  [type] $data   [description]
 * @return [type]         [description]
 */
function verify_sign($secret, $data)
{
    // 验证参数中是否有签名
    if (!isset($data['sign']) || !$data['sign']) {
        throw new \Exception('数据未签名', 1);
    }
    if (!isset($data['timestamp']) || !$data['timestamp']) {
        throw new \Exception('发送的数据参数不合法', 1);
    }
    // 验证请求， 10分钟失效
    if (time() - $data['timestamp'] > 600) {
        throw new \Exception('验证失效， 请重新发送请求', 1);
    }
    $sign = $data['sign'];
    unset($data['sign']);
    ksort($data);
    $params = http_build_query($data);
    // $secret是通过key在api的数据库中查询得到
    $sign2 = md5($params . $secret);
    if ($sign == $sign2) {
        return true;
    } else {
        throw new \Exception('请求不合法', 1);
    }
}
