<?php

namespace app\admin\controller\login;

use app\admin\controller\Login;
use app\admin\model\SystemAdminLogin;
use app\admin\model\SystemLogin;
use app\common\service\LoginType\WechatOfficialAccountWebviewService;
use app\common\tools\QrocdeToos;
use Overtrue\Socialite\Exceptions\AuthorizeFailedException;
use think\facade\Cache;
use think\facade\Log;

class WechatOfficialAccountWebview extends Login
{
    public function index()
    {
        $id = $this->request->param('id');

        if (empty($id)) {
            $this->error('参数错误', '', __url('admin/Login/index'));
        }

        $login_code = uniqid();

        Cache::set('login_code_' . $login_code, ['id' => $id], 60 * 5);

        $this->assign('login_code', $login_code);

        $url = __url('/admin/login.wechatOfficialAccountWebview/wechatAuth', ['login_code' => $login_code], false, true);

        dump($url);

        $qrcode_content = QrocdeToos::generate($url, true);

        $this->assign('qrcode_content', $qrcode_content);

        return $this->fetch();
    }

    public function wechatAuth()
    {
        $login_state = '';
        $login_code = $this->request->param('login_code');

        if (empty($login_code)) {
            $this->error('参数错误', '', __url('admin/Login/index'));
        }

        $login_info = Cache::get('login_code_' . $login_code);

        if (empty($login_info)) {
            $login_state = '登录信息已过期';
            goto SHOW_PAGE;
        }

        $id = $login_info['id'];

        $model_login = SystemLogin::find($id);

        if (empty($model_login)) {
            $login_state = '登录信息不存在';
            goto SHOW_PAGE;
        }

        $app = WechatOfficialAccountWebviewService::getApplication($model_login);

        $code = $this->request->param('code');

        if (empty($code)) {
            $redirect_url = $app->getOAuth()->scopes(['snsapi_base'])->redirect($this->request->url(true));

            return redirect($redirect_url);
        }

        try {
            $user = $app->getOAuth()->userFromCode($code);
        } catch (AuthorizeFailedException $th) {
            Log::error($th);
            $login_state = '授权失败';
            goto SHOW_PAGE;
        }

        dump($user);

        $openid = $user->getId();

        $user_info = $user->getRaw();

        $model_admin_login = SystemAdminLogin::where('type',$model_login->type)
        ->where('login_id',$model_login->id)
        ->where('openid',$openid)->find();

        if (empty($model_admin_login)) {
            $model_admin_login = new SystemAdminLogin();
            $model_admin_login->type = $model_login->type;
            $model_admin_login->login_id = $model_login->id;
            $model_admin_login->openid = $openid;   
            $model_admin_login->admin_id = 0; 
            $model_admin_login->save();
        }

        $model_admin_login->info = $user_info;
        $model_admin_login->save();

        
        SHOW_PAGE:
        $this->assign('login_code', $login_code);
        $this->assign('login_state', $login_state);

        return $this->fetch();
    }
}
