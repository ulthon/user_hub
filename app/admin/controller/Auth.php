<?php

namespace app\admin\controller;

use app\admin\model\AppApplication;
use app\admin\model\AppApplicationAdmin;
use app\admin\model\AppApplicationAdminRecord;
use app\admin\model\AppApplicationDomain;
use app\common\controller\AdminController;

class Auth extends AdminController
{
    public function show()
    {
        $type = $this->request->param('type', 'confirm');

        $key = $this->request->param('key');
        $url = $this->request->param('url');

        // TODO:验证url
        $url = urldecode($url);

        $model_app = AppApplication::where('key', $key)->find();

        if (empty($model_app)) {
            return $this->error('应用不存在或者key错误', $url);
        }

        $url_info = parse_url($url);

        if (!empty($url_info['query'])) {
            parse_str($url_info['query'], $query);
            $url_info['query'] = $query;
        }

        if ($this->sessionAdmin->dev_status != 1) {
            $url_host = $url_info['host'];
            $model_domain = AppApplicationDomain::where('app_id', $model_app->id)
                ->where('domain', $url_host)
                ->find();

            if (empty($model_domain)) {
                return $this->error('回调域名错误', $url);
            }
        }

        if ($type == 'quick' || $type == 'confirmd') {
            $model_app_user_record = $this->buildAuthCode($model_app->id, 1);

            if (!isset($url_info['query'])) {
                $url_info['query'] = [];
            }
            $url_info['query']['code'] = $model_app_user_record->code;

            $url_info['query'] = http_build_query($url_info['query']);

            $backurl = unparse_url($url_info);

            return $this->redirect($backurl);
        } elseif ($type == 'confirm') {
            // TODO:需要用户确认的授权,返回授权页面,点击确认

            $this->assign('model_app', $model_app);

            $this->assign('auth_link', url('', ['type' => 'confirmd', 'key' => $key, 'url' => $url]));

            return $this->fetch();
        }
    }

    public function buildAuthCode($app_id, $status = 0)
    {
        $model_app_user = AppApplicationAdmin::where('app_id', $app_id)
            ->where('admin_id', $this->sessionAdmin->id)
            ->find();

        if (empty($model_app_user)) {
            $model_app_user = AppApplicationAdmin::create([
                'admin_id' => $this->sessionAdmin->id,
                'app_id' => $app_id,
                'status' => $status,
            ]);
        }

        $model_app_user_record = AppApplicationAdminRecord::create([
            'code' => uniqid(),
            'admin_id' => $this->sessionAdmin->id,
            'app_id' => $app_id,
        ]);

        return $model_app_user_record;
    }
}
