<?php

namespace app\admin\controller;

use app\admin\model\SystemAdmin;
use app\common\service\SafeStateService;
use base\admin\controller\LoginBase;
use think\facade\Event;
use think\facade\Session;

/**
 * Class Login.
 */
class Login extends LoginBase
{
    /**
     * 用户登录.
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        event_response('AdminLoginIndex', [
            'controller' => $this,
        ]);

        $back_url = $this->request->param('back_url');

        if (!empty($back_url)) {
            Session::set('back-url', $back_url);
        } else {
            $back_url = Session::get('back-url');
        }

        if (empty($back_url)) {
            $back_url = null;
        }

        $captcha = 0;

        $safe_state = SafeStateService::get();

        if ($safe_state != 'safe') {
            $captcha = 1;
        }

        if ($this->request->isPost()) {
            $post = $this->request->post();
            $rule = [
                'username|用户名' => 'require',
                'password|密码' => 'require',
                'keep_login|是否保持登录' => 'require',
            ];
            $captcha == 1 && $rule['captcha|验证码'] = 'require|captcha';
            $this->validate($post, $rule);
            $admin = SystemAdmin::where(['username' => $post['username']])->find();
            if (empty($admin)) {
                SafeStateService::set(SafeStateService::SAFE_STATE_RISK);
                $this->result(null, 501, '用户不存在');
            }
            if (md5($post['password'] . $admin['salt']) != $admin->password) {
                SafeStateService::set(SafeStateService::SAFE_STATE_RISK);
                $this->result(null, 501, '密码输入有误');
            }
            if ($admin->status == 0) {
                SafeStateService::set(SafeStateService::SAFE_STATE_BAN);
                $this->result(null, 501, '账号已被禁用');
            }
            $admin->login_num += 1;
            $admin->save();

            Event::trigger('AdminLoginSuccess', $admin);

            $admin = $admin->toArray();
            unset($admin['password']);
            $admin['expire_time'] = $post['keep_login'] == 1 ? true : time() + 7200;
            session('admin', $admin);
            SafeStateService::set(SafeStateService::SAFE_STATE_SAFE);

            Session::delete('back-url');
            $this->success('登录成功', '', $back_url);
        }
        $this->assign('captcha', $captcha);
        $this->assign('demo', $this->isDemo);

        return $this->fetch();
    }

    public function register()
    {
        $captcha = 1;

        if ($this->request->isPost()) {
            $post = $this->request->post();
            $rule = [
                'username|用户名' => 'require',
                'password|密码' => 'require',
            ];
            $captcha == 1 && $rule['captcha|验证码'] = 'require|captcha';
            $this->validate($post, $rule);
            $admin = SystemAdmin::where(['username' => $post['username']])->find();
            if (!empty($admin)) {
                $this->error('用户已存在，请前往登录');
            }

            $admin = new SystemAdmin();
            $admin->username = $post['username'];

            $post['salt'] = mt_rand(100000, 999999);
            $admin->salt = $post['salt'];

            $admin->password = md5($post['password'] . $post['salt']);
            $admin->uid = uniqid();

            $admin->save();

            $this->success('注册成功，请前往登录');
        }

        $this->assign('captcha', $captcha);

        return $this->fetch();
    }
}
