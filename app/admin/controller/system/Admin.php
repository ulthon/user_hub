<?php

namespace app\admin\controller\system;

use app\admin\service\annotation\ControllerAnnotation;
use app\admin\service\annotation\NodeAnotation;
use base\admin\controller\system\AdminBase;

/**
 * Class Admin.
 * @ControllerAnnotation(title="管理员管理")
 *
 * @NodeAnotation(title="自定义权限标识符",name="customFlag")
 */
class Admin extends AdminBase
{
    /**
     * @NodeAnotation(title="编辑")
     */
    public function password($id)
    {
        $row = $this->model->find($id);
        empty($row) && $this->error('数据不存在');
        if ($this->request->isAjax()) {
            $this->checkPostRequest();
            $post = $this->request->post();
            $rule = [
                'password|登录密码' => 'require',
                'password_again|确认密码' => 'require',
            ];
            $this->validate($post, $rule);
            if ($post['password'] != $post['password_again']) {
                $this->error('两次密码输入不一致');
            }
            try {
                $salt = mt_rand(100000, 999999);
                $save = $row->save([
                    'salt' => $salt,
                    'password' => md5($post['password'] . $salt),
                ]);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            $save ? $this->success('保存成功') : $this->error('保存失败');
        }
        $row->auth_ids = explode(',', $row->auth_ids);
        $this->assign('row', $row);

        return $this->fetch();
    }
}
