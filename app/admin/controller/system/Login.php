<?php

namespace app\admin\controller\system;

use app\admin\service\annotation\ControllerAnnotation;
use app\common\controller\AdminController;
use think\App;
use think\facade\Config;

/**
 * @ControllerAnnotation(title="system_login")
 */
class Login extends AdminController
{
    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\SystemLogin();

        $this->assign('select_list_status', $this->model::SELECT_LIST_STATUS, true);

        $this->assign('select_list_is_support_temp', $this->model::SELECT_LIST_IS_SUPPORT_TEMP, true);

        $this->assign('list_login_type', Config::get('login.login_type'));
    }
}
