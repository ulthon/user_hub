<?php

namespace app\admin\controller\app;

use app\admin\service\annotation\ControllerAnnotation;
use app\common\controller\AdminController;
use think\App;

/**
 * @ControllerAnnotation(title="app_application")
 */
class Application extends AdminController
{
    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\AppApplication();

        $this->assign('select_list_status', $this->model::SELECT_LIST_STATUS, true);
    }
}
