<?php

namespace app\admin\controller\app;

use app\common\controller\AdminController;
use app\admin\service\annotation\ControllerAnnotation;
use app\admin\service\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="app_application_admin")
 */
class ApplicationAdmin extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\AppApplicationAdmin();
        
        $this->assign('select_list_status', $this->model::SELECT_LIST_STATUS, true);

    }

    
    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->withJoin(['systemAdmin','appApplication','appApplicationAdminGroup'], 'LEFT')
                ->where($where)
                ->count();
            $list = $this->model
                ->withJoin(['systemAdmin','appApplication','appApplicationAdminGroup'], 'LEFT')
                ->where($where)
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    
    
    /**
     * @NodeAnotation(title="导出")
     */
    public function export()
    {
        list($page, $limit, $where) = $this->buildTableParames();


        $this->model = $this->model->withJoin(['systemAdmin','appApplication','appApplicationAdminGroup'], 'LEFT');

        $fields = $this->request->param('fields', '{}', null);
        $image_fields = $this->request->param('image_fields', '{}', null);
        $select_fields = $this->request->param('select_fields', '{}', null);
        $date_fields = $this->request->param('date_fields', '{}', null);

        $fields = json_decode($fields, true);
        $image_fields = json_decode($image_fields, true);
        $select_fields = json_decode($select_fields, true);
        $date_fields = json_decode($date_fields, true);

        $content = \app\common\tools\ExportTools::excel($this->model, $where, $fields, $image_fields, $select_fields, $date_fields);

        $export_file_name = $this->exportFileName;

        if (empty($export_file_name)) {
            $export_file_name = $this->model->getName();
        }

        return download($content, $export_file_name . date('YmdHis') . '.xlsx', true);
    }
}