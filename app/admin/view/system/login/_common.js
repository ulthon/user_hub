var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'system.login/index',
    addUrl: 'system.login/add',
    editUrl: 'system.login/edit',
    deleteUrl: 'system.login/delete',
    exportUrl: 'system.login/export',
    modifyUrl: 'system.login/modify',
};