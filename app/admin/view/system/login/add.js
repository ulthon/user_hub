$(function () {
    ua.listen();

    var appElem = $('input[name="config"]').closest('.property-input-container');

    var app = appElem[0].__vue__;

    layui.form.on('radio(type-radio)', function (data) {

        var elem = data.elem;
        var config = $(elem).closest('td').data('config-tpl');
        config = JSON.parse(decodeURIComponent(config));

        app.listItem = config;

        var field = $(elem).closest('td').data('field-tpl');
        field = JSON.parse(decodeURIComponent(field));

        console.log(field);
        app.field = field;

    });
});