$(function(){
     ua.table.render({
        init: init,
        cols: [[
            {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'create_time', title: '创建时间'},                    {field: 'title', title: '名称'},                    {field: 'type', title: '类型'},                    {field: 'sort', title: '排序', edit: 'text'},                    {field: 'status', search: 'select', selectList: ua.getDataBrage('select_list_status'), title: '状态', templet: ua.table.switch},                    {field: 'is_support_temp', search: 'select', selectList: ua.getDataBrage('select_list_is_support_temp'), title: '支持临时创建'},                    {field: 'config', title: '配置'},                    {width: 250, title: '操作', templet: ua.table.tool , fixed:'right'},
        ]],
    });

    ua.listen();
})