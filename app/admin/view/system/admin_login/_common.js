var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'system.admin_login/index',
    addUrl: 'system.admin_login/add',
    editUrl: 'system.admin_login/edit',
    deleteUrl: 'system.admin_login/delete',
    exportUrl: 'system.admin_login/export',
    modifyUrl: 'system.admin_login/modify',
};