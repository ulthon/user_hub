$(function(){
     ua.table.render({
        init: init,
        cols: [[
            {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'create_time', title: '创建时间'},                    {field: 'type', title: '类型'},                    {field: 'login_id', title: '登陆类型'},                    {field: 'openid', title: '三方主键'},                    {field: 'info', title: '三方信息'},                    {field: 'systemLogin.id', title: ''},                    {field: 'systemLogin.create_time', title: '创建时间'},                    {field: 'systemLogin.title', title: '名称'},                    {field: 'systemLogin.type', title: '类型'},                    {field: 'systemLogin.sort', title: '排序'},                    {field: 'systemLogin.status', title: '状态', templet: ua.table.switch},                    {field: 'systemLogin.is_support_temp', title: '支持临时创建'},                    {field: 'systemLogin.config', title: '配置'},                    {width: 250, title: '操作', templet: ua.table.tool , fixed:'right'},
        ]],
    });

    ua.listen();
})