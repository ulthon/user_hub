$(function () {
    ua.listen(function (data) {

        return data;
    }, function (res) {
        ua.msg.success(res.msg, function () {
            window.location = ua.url('login/index');
        });
    }, function (res) {
        ua.msg.error(res.msg, function () {
            $('#refreshCaptcha').trigger("click");
        });
    });
});