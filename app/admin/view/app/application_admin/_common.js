var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'app.application_admin/index',
    addUrl: 'app.application_admin/add',
    editUrl: 'app.application_admin/edit',
    deleteUrl: 'app.application_admin/delete',
    exportUrl: 'app.application_admin/export',
    modifyUrl: 'app.application_admin/modify',
};