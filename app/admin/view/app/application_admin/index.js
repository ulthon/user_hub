$(function () {
    ua.table.render({
        init: init,
        toolbar: ['refresh', 'export'],
        cols: [[
            { type: 'checkbox' },
            { field: 'id', title: 'id' },
            { field: 'create_time', title: '首次授权时间', },
            { field: 'update_time', title: '最后授权时间', },

            { field: 'admin_id', title: '用户', trueHide: true, defaultSearchValue: ua.getQueryVariable('admin_id') },
            { field: 'app_id', title: '应用', trueHide: true, defaultSearchValue: ua.getQueryVariable('app_id') },

            {field: 'status', search: 'select', selectList: ua.getDataBrage('select_list_status'), title: '状态', templet: ua.table.switch},
            {field: 'sort', title: '排序', edit: 'text'},
            {field: 'group_id', title: '分组'},

            { field: 'systemAdmin.username', title: '用户登录名' },
            { field: 'systemAdmin.nickname', title: '昵称' },
            { field: 'systemAdmin.head_img', title: '头像' },

            { field: 'appApplication.title', title: '名称' },
            { field: 'appApplication.comment', title: '备注' },

        ]],
    });

    ua.listen();
});