var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'app.message/index',
    addUrl: 'app.message/add',
    editUrl: 'app.message/edit',
    deleteUrl: 'app.message/delete',
    exportUrl: 'app.message/export',
    modifyUrl: 'app.message/modify',
};