var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'app.application/index',
    addUrl: 'app.application/add',
    editUrl: 'app.application/edit',
    deleteUrl: 'app.application/delete',
    exportUrl: 'app.application/export',
    modifyUrl: 'app.application/modify',
};