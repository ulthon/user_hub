$(function () {
    ua.table.render({
        init: init,
        cols: [[
            { type: 'checkbox' },
            { field: 'id', title: 'id' },
            { field: 'title', title: '名称' },
            { field: 'comment', title: '备注', },
            { field: 'create_time', title: '创建时间' },
            { field: 'key', title: 'key' },
            { field: 'secret', title: 'secret', minWidth: 280 },
            { field: 'status', search: 'select', selectList: ua.getDataBrage('select_list_status'), title: '状态', templet: ua.table.switch },
            {
                width: 320, title: '操作', templet: ua.table.tool, fixed: 'right', operat: [
                    'edit',
                    [
                        {
                            text: '绑定域名',
                            url: 'app.application_domain/index',
                            class: 'layui-btn layui-btn-success layui-btn-xs',
                            method: 'open',
                            field: function (data, option) {
                                return {
                                    app_id: data.id,

                                };
                            }
                        },
                        {
                            text: '授权用户',
                            url: 'app.application_admin/index',
                            class: 'layui-btn layui-btn-success layui-btn-xs',
                            method: 'open',
                            field: function (data, option) {
                                return {
                                    app_id: data.id,

                                };
                            }
                        },
                        {
                            text: '授权记录',
                            url: 'app.application_admin_record/index',
                            class: 'layui-btn layui-btn-success layui-btn-xs',
                            method: 'open',
                            field: function (data, option) {
                                return {
                                    app_id: data.id,

                                };
                            }
                        },
                    ],
                    'delete'
                ]
            },

        ]],
    });

    ua.listen();
});