$(function () {
    ua.table.render({
        init: init,
        cols: [[
            { type: 'checkbox' },
            { field: 'id', title: 'id' },
            { field: 'app_id', title: '应用', trueHide: true, defaultSearchValue: ua.getQueryVariable('app_id') },
            { field: 'domain', title: '域名', minWidth: 180 },
            { field: 'appApplication.id', title: '', trueHide: true },
            { field: 'appApplication.title', title: '名称', minWidth: 120 },
            { field: 'appApplication.comment', title: '备注' },
            {
                width: 250, title: '操作', templet: ua.table.tool, fixed: 'right', operat: [
                    'edit',
                    [
                        {
                            text: '访问域名',
                            url: function (data) {
                                return 'http://' + data.domain;
                            },
                            class: 'layui-btn layui-btn-xs',
                            method: 'blank',
                            field: null

                        },
                    ],
                    'delete'
                ]
            },

        ]],
    });

    ua.listen();
});