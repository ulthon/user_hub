var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'app.application_domain/index',
    addUrl: 'app.application_domain/add',
    editUrl: 'app.application_domain/edit',
    deleteUrl: 'app.application_domain/delete',
    exportUrl: 'app.application_domain/export',
    modifyUrl: 'app.application_domain/modify',
};