$(function(){
     ua.table.render({
        init: init,
        cols: [[
            {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'create_time', title: 'create_time'},                    {field: 'admin_id', title: '用户'},                    {field: 'sort', title: '排序', edit: 'text'},                    {field: 'title', title: '名称'},                    {field: 'systemAdmin.id', title: ''},                    {field: 'systemAdmin.auth_ids', title: '角色权限ID'},                    {field: 'systemAdmin.head_img', title: '头像'},                    {field: 'systemAdmin.username', title: '用户登录名'},                    {field: 'systemAdmin.password', title: '用户密码'},                    {field: 'systemAdmin.salt', title: ''},                    {field: 'systemAdmin.phone', title: '联系手机号'},                    {field: 'systemAdmin.remark', title: '备注说明', templet: ua.table.text},                    {field: 'systemAdmin.login_num', title: '登录次数'},                    {field: 'systemAdmin.sort', title: '排序', edit: 'text'},                    {field: 'systemAdmin.status', title: '状态', templet: ua.table.switch},                    {field: 'systemAdmin.create_time', title: '创建时间'},                    {field: 'systemAdmin.uid', title: ''},                    {field: 'systemAdmin.nickname', title: ''},                    {field: 'systemAdmin.last_login_time', title: ''},                    {field: 'systemAdmin.intro', title: ''},                    {field: 'systemAdmin.sex', title: ''},                    {field: 'systemAdmin.super_admin', title: '超级管理员'},                    {width: 250, title: '操作', templet: ua.table.tool , fixed:'right'},
        ]],
    });

    ua.listen();
})