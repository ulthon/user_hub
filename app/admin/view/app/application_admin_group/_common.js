var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'app.application_admin_group/index',
    addUrl: 'app.application_admin_group/add',
    editUrl: 'app.application_admin_group/edit',
    deleteUrl: 'app.application_admin_group/delete',
    exportUrl: 'app.application_admin_group/export',
    modifyUrl: 'app.application_admin_group/modify',
};