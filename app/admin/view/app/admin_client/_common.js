var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'app.admin_client/index',
    addUrl: 'app.admin_client/add',
    editUrl: 'app.admin_client/edit',
    deleteUrl: 'app.admin_client/delete',
    exportUrl: 'app.admin_client/export',
    modifyUrl: 'app.admin_client/modify',
};