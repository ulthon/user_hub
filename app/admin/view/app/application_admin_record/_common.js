var init = {
    tableElem: '#currentTable',
    tableRenderId: 'currentTableRenderId',
    indexUrl: 'app.application_admin_record/index',
    addUrl: 'app.application_admin_record/add',
    editUrl: 'app.application_admin_record/edit',
    deleteUrl: 'app.application_admin_record/delete',
    exportUrl: 'app.application_admin_record/export',
    modifyUrl: 'app.application_admin_record/modify',
};