$(function () {
    ua.table.render({
        init: init,
        toolbar: ['refresh', 'export'],
        cols: [[
            { type: 'checkbox' },
            { field: 'id', title: 'id' },
            { field: 'create_time', title: 'create_time' },
            { field: 'code', title: '记录码' },

            { field: 'admin_id', title: '用户', trueHide: true, defaultSearchValue: ua.getQueryVariable('admin_id') },
            { field: 'app_id', title: '应用', trueHide: true, defaultSearchValue: ua.getQueryVariable('app_id') },


            { field: 'systemAdmin.username', title: '用户登录名' },
            { field: 'systemAdmin.nickname', title: '昵称' },
            { field: 'systemAdmin.head_img', title: '头像' },

            { field: 'appApplication.title', title: '名称' },
            { field: 'appApplication.comment', title: '备注' },

        ]],
    });

    ua.listen();
});