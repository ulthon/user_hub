<?php

namespace app\admin\model;

use app\common\model\TimeModel;

/**
 * @property int $id id
 * @property AppApplication $appApplication 应用
 * @property string $domain 域名
 */
class AppApplicationDomain extends TimeModel
{
    protected $name = 'app_application_domain';

    protected $deleteTime = false;

    public function appApplication()
    {
        return $this->belongsTo('\app\admin\model\AppApplication', 'app_id', 'id');
    }
}
