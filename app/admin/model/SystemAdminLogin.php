<?php

namespace app\admin\model;

use app\common\model\TimeModel;

/**
 * @property int $id id 
 * @property int $create_time 创建时间 
 * @property string $type 类型 
 * @property \app\admin\model\SystemLogin $systemLogin 登陆类型 
 * @property string $openid 三方主键 
 * @property string $info 三方信息 
 * @property int $admin_id 账号 
 */
class SystemAdminLogin extends TimeModel
{

    protected $name = "system_admin_login";

    protected $deleteTime = "delete_time";

    
    
    
    public function systemLogin()
    {
        return $this->belongsTo('\app\admin\model\SystemLogin', 'login_id', 'id');
    }

    public function setInfoAttr($value)
    {
        return json_encode($value);
    }


}