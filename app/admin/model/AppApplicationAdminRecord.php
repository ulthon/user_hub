<?php

namespace app\admin\model;

use app\common\model\TimeModel;

/**
 * @property int $id id
 * @property int $create_time create_time
 * @property string $code 记录码
 * @property int $status 状态 0:发起授权,1:已授权
 * @property SystemAdmin $systemAdmin 用户
 * @property AppApplication $appApplication 应用
 */
class AppApplicationAdminRecord extends TimeModel
{
    protected $name = 'app_application_admin_record';

    protected $deleteTime = 'delete_time';

    public const SELECT_LIST_STATUS = ['0' => '发起授权', '1' => '已授权'];

    public function systemAdmin()
    {
        return $this->belongsTo('\app\admin\model\SystemAdmin', 'admin_id', 'id');
    }

    public function appApplication()
    {
        return $this->belongsTo('\app\admin\model\AppApplication', 'app_id', 'id');
    }
}
