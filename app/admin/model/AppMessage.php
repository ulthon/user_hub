<?php

namespace app\admin\model;

use app\common\model\TimeModel;

/**
 * @property int $id id 
 * @property int $create_time create_time 
 * @property \app\admin\model\SystemAdmin $systemAdmin 用户 
 * @property \app\admin\model\AppApplication $appApplication 应用 
 * @property string $title 标题 
 * @property string $content 内容 
 * @property string $option_setting 可操作设置 
 */
class AppMessage extends TimeModel
{

    protected $name = "app_message";

    protected $deleteTime = "delete_time";

    
    
    
    public function systemAdmin()
    {
        return $this->belongsTo('\app\admin\model\SystemAdmin', 'admin_id', 'id');
    }

    public function appApplication()
    {
        return $this->belongsTo('\app\admin\model\AppApplication', 'app_id', 'id');
    }


}