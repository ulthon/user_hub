<?php

namespace app\admin\model;

use app\common\model\TimeModel;

/**
 * @property int $id id 
 * @property int $create_time 创建时间 
 * @property \app\admin\model\SystemAdmin $systemAdmin 用户 
 * @property string $client_type 类型 
 * @property int $login_time 登陆时间 
 * @property string $client_id 客户端标识符 
 * @property int $status 状态 1:离线
 */
class AppAdminClient extends TimeModel
{

    protected $name = "app_admin_client";

    protected $deleteTime = "delete_time";

    
    public const SELECT_LIST_STATUS = ['1'=>'离线',];

    
    
    public function systemAdmin()
    {
        return $this->belongsTo('\app\admin\model\SystemAdmin', 'admin_id', 'id');
    }


}