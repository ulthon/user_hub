<?php

namespace app\admin\model;

use app\common\model\TimeModel;

/**
 * @property int $id id 
 * @property int $create_time create_time 
 * @property \app\admin\model\SystemAdmin $systemAdmin 用户 
 * @property \app\admin\model\AppApplication $appApplication 应用 
 * @property int $status 状态 0:正常,1:收藏,2:拉黑
 * @property int $sort 排序 
 * @property \app\admin\model\AppApplicationAdminGroup $appApplicationAdminGroup 分组 
 */
class AppApplicationAdmin extends TimeModel
{

    protected $name = "app_application_admin";

    protected $deleteTime = "delete_time";

    
    public const SELECT_LIST_STATUS = ['0'=>'正常','1'=>'收藏','2'=>'拉黑',];

    
    
    public function systemAdmin()
    {
        return $this->belongsTo('\app\admin\model\SystemAdmin', 'admin_id', 'id');
    }

    public function appApplication()
    {
        return $this->belongsTo('\app\admin\model\AppApplication', 'app_id', 'id');
    }

    public function appApplicationAdminGroup()
    {
        return $this->belongsTo('\app\admin\model\AppApplicationAdminGroup', 'group_id', 'id');
    }


}