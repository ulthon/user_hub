<?php

namespace app\admin\model;

use app\common\model\TimeModel;

/**
 * @property int $id id
 * @property string $title 名称
 * @property string $comment 备注
 * @property int $create_time 创建时间
 * @property string $key key
 * @property string $secret secret
 * @property int $status 状态 0:禁用,1:启用
 */
class AppApplication extends TimeModel
{
    protected $name = 'app_application';

    protected $deleteTime = 'delete_time';

    public const SELECT_LIST_STATUS = ['0' => '禁用', '1' => '启用'];
}
