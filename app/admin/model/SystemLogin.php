<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\facade\Config;

/**
 * @property int $id id
 * @property int $create_time 创建时间
 * @property string $title 名称
 * @property string $type 类型
 * @property string $sort 排序
 * @property int $status 状态 0:禁用,1:启用
 * @property int $is_support_temp 支持临时创建 0:禁用,1:启用
 * @property string $config 配置
 * @property int $config_copy_from_id 复制配置
 */
class SystemLogin extends TimeModel
{
    protected $name = 'system_login';

    protected $deleteTime = 'delete_time';

    public const SELECT_LIST_STATUS = ['0' => '禁用', '1' => '启用'];

    public const SELECT_LIST_IS_SUPPORT_TEMP = ['0' => '禁用', '1' => '启用'];

    public function getLoginServiceAttr()
    {
        return Config::get('login.login_type.' . $this->getAttr('type'));
    }

    public function getConfigArrayAttr()
    {
        $value = $this->getAttr('config');

        return json_decode($value, true);
    }

    public function getConfigSettingAttr()
    {
        $config = $this->getAttr(('config_array'));

        $setting = [];

        foreach ($config as $key => $value) {
            $setting[$value['name']] = $value['value'];
        }

        return $setting;
    }
}
