<?php

namespace app\admin\model;

use app\common\model\TimeModel;

/**
 * @property int $id id 
 * @property int $create_time create_time 
 * @property \app\admin\model\SystemAdmin $systemAdmin 用户 
 * @property int $sort 排序 
 * @property string $title 名称 
 */
class AppApplicationAdminGroup extends TimeModel
{

    protected $name = "app_application_admin_group";

    protected $deleteTime = "delete_time";

    
    
    
    public function systemAdmin()
    {
        return $this->belongsTo('\app\admin\model\SystemAdmin', 'admin_id', 'id');
    }


}