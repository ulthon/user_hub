<?php

// 事件定义文件

use app\common\event\AdminLayoutRequireAfter\AdminRequireEvent;
use app\common\event\AdminLoginForget\ForgetEvent;
use app\common\event\AdminLoginType\ExtraTypeEvent;
use app\common\event\AdminLoginType\RegisterEvent;

return [
    'bind' => [
    ],

    'listen' => [
        'AppInit' => [],
        'HttpRun' => [],
        'HttpEnd' => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'AdminLoginSuccess' => [

        ],
        'AdminLoginType' => [
            RegisterEvent::class,
            ExtraTypeEvent::class,
        ],
        'AdminLoginForget' => [
            ForgetEvent::class,
        ],
        'AdminLayoutRequireAfter' => [
            AdminRequireEvent::class,
        ],
    ],

    'subscribe' => [
    ],
];
