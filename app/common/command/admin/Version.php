<?php

declare(strict_types=1);

namespace app\common\command\admin;

use base\common\command\admin\VersionBase;

class Version extends VersionBase
{
    public const PRODUCT_VERSION = 'v1.0.6';

    public const COMMENT = [
        '增加密码盐',
        '发布新版本',
    ];
}
