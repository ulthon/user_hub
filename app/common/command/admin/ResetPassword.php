<?php

declare(strict_types=1);

namespace app\common\command\admin;

use app\admin\model\SystemAdmin;
use base\common\command\admin\ResetPasswordBase;
use think\console\Input;
use think\console\Output;

class ResetPassword extends ResetPasswordBase
{
    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $output->writeln('admin:reset:password');

        $model_admin = SystemAdmin::where('super_admin', 1)->find();
        if (empty($model_admin)) {
            $output->writeln('管理员不存在');

            return false;
        }

        $password = $input->getOption('password');

        $salt = $model_admin->salt;

        if (is_null($password)) {
            $password = uniqid();
        }

        $model_admin->save([
            'password' => md5($password . $salt),
        ]);

        $output->writeln('匹配的账号名:' . $model_admin->username);
        $output->writeln('密码修改为:' . $password);
    }
}
