<?php

namespace app\common\tools;

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\PngWriter;

class QrocdeToos
{
    public static function generate($content, $save_name = null)
    {
        if (is_null($save_name)) {
            $save_name = PathTools::publicBuildSaveName('t/' . date('Ymd') . 'qrcode_' . uniqid() . '.png');
            $file_path = PathTools::publicPath($save_name);

            if (file_exists($file_path)) {
                return $save_name;
            }
        }

        $writer = new PngWriter();

        // Create QR code
        $qrCode = QrCode::create($content)
            ->setEncoding(new Encoding('UTF-8'))
            ->setSize(300)
            ->setMargin(10)

            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);

        if ($save_name === false) {
            return $result;
        }

        if ($save_name === true) {
            return $result->getDataUri();
        }

        $result->saveToFile($file_path);

        return $save_name;
    }
}
