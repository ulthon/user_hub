<?php

namespace app\common\event\AdminLayoutRequireAfter;

use think\facade\View;

class AdminRequireEvent
{
    public function handle()
    {
        return [
            'view_content' => View::layout(false)->fetch('common/ext/_require_after'),
        ];
    }
}
