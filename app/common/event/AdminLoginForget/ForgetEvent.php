<?php

namespace app\common\event\AdminLoginForget;

class ForgetEvent
{
    public function handle()
    {
        return [
            'view_replace' => '<a href="" class="forget-password">忘记密码？</a>',
            'view_replace_js' => '//',
        ];
    }
}
