<?php

namespace app\common\event\AdminLoginType;

use app\admin\model\SystemLogin;
use think\facade\View;

class ExtraTypeEvent
{
    public function handle()
    {
        $list_login = SystemLogin::order('sort', 'asc')->where('status', 1)->select();

        View::assign('list_login', $list_login);

        $content = '';
        $content = View::layout(false)->fetch('login/ext/extra');

        // 事件监听处理
        return [
            'view_content' => $content,
        ];
    }
}
