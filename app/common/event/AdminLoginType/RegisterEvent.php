<?php

namespace app\common\event\AdminLoginType;

use think\facade\View;

class RegisterEvent
{
    public function handle()
    {
        $content = '';

        $content = View::layout(false)->fetch('login/ext/register');

        // 事件监听处理
        return [
            'view_content' => $content,
        ];
    }
}
