<?php

namespace app\common\service;

use app\common\constants\AdminConstant;
use base\common\service\AuthServiceBase;
use think\facade\Config;

/**
 * 权限验证服务
 * Class AuthService.
 */
class AuthService extends AuthServiceBase
{
    /**
     * 检测检测权限.
     * @param null $node
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function checkNode($node = null)
    {
        // 判断是否为超级管理员
        if ($this->adminId == AdminConstant::SUPER_ADMIN_ID) {
            return true;
        }

        if($this->adminInfo['super_admin'] == 1){
            return true;
        }

        // 判断权限验证开关
        if ($this->config['auth_on'] == false) {
            return true;
        }
        // 判断是否需要获取当前节点
        if (empty($node)) {
            $node = $this->getCurrentNode();
        } else {
            $node = $this->parseNodeStr($node);
        }
        // 判断是否加入节点控制，优先获取缓存信息
        if (!isset($this->nodeList[$node])) {
            return Config::get('admin.default_auth_check');
        }
        $nodeInfo = $this->nodeList[$node];
        if ($nodeInfo['is_auth'] == 0) {
            return true;
        }
        // 用户验证，优先获取缓存信息
        if (empty($this->adminInfo) || $this->adminInfo['status'] != 1 || empty($this->adminInfo['auth_ids'])) {
            return false;
        }
        // 判断该节点是否允许访问
        if (in_array($node, $this->adminNode)) {
            return true;
        }

        return false;
    }

}
