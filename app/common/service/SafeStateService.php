<?php

namespace app\common\service;

use think\facade\Session;

class SafeStateService
{
    const SAFE_STATE = 'safe_state';

    const SAFE_STATE_SAFE = 'safe';
    const SAFE_STATE_RISK = 'risk';
    const SAFE_STATE_BAN = 'ban';

    public static function get()
    {
        $safe_state = Session::get('safe_state');

        if (empty($safe_state)) {
            $safe_state = self::SAFE_STATE_SAFE;

            Session::set('safe_state', $safe_state);
        }

        return $safe_state;
    }

    public static function set($state)
    {
        Session::set('safe_state', $state);

        return $state;
    }
}
