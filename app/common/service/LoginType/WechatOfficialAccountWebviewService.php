<?php

namespace app\common\service\LoginType;

use app\admin\model\SystemLogin;
use app\common\service\LoginTypeService;
use EasyWeChat\OfficialAccount\Application;

class WechatOfficialAccountWebviewService extends LoginTypeService
{
    public const NAME = '微信公众号网页授权登录';

    public const SHORT_NAME = '微信授权登录';

    public const DESC = '使用微信扫码，在打开的页面中点击“确认”按钮，即可完成授权登录。';

    public const TYPE_NAME = 'WechatOfficialAccountWebviewService';

    public static function getConfigTpl()
    {
        return [
            [
                'name' => 'appid',
                'placeholder' => '请输入AppID',
            ],
            [
                'name' => 'appsecret',
                'placeholder' => '请输入AppSecret',
            ],
        ];
    }

    public static function getLoginUrl(SystemLogin $model_login)
    {
        return url('admin/login.WechatOfficialAccountWebview/index', ['id' => $model_login->id]);
    }

    public static function getApplication(SystemLogin $model_login)
    {
        $setting = $model_login->config_setting;

        $config = [];
        $config['app_id'] = $setting['appid'];
        $config['secret'] = $setting['appsecret'];

        $app = new Application($config);

        return $app;
    }
}
