<?php

namespace app\common\service;

use app\admin\model\SystemLogin;

class LoginTypeService
{
    public const NAME = '登录名称';

    public const SHORT_NAME = '登录简称';

    public const DESC = '登录描述';

    public const TYPE_NAME = '';

    public static function getConfigTpl()
    {
        return [];
    }

    public static function getFieldTpl()
    {
        return [
            [
                'key' => 'name',
                'title' => '名称',
                'type' => 'text',
            ],
            [
                'key' => 'value',
                'title' => '数据',
                'type' => 'textarea',
            ],
        ];
    }

    public static function getLoginUrl(SystemLogin $model_login)
    {
        return '';
    }
}
