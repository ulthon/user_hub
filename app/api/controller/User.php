<?php

declare(strict_types=1);

namespace app\api\controller;

use app\admin\model\AppApplicationAdminRecord;
use app\admin\model\SystemAdmin;
use app\model\User as ModelUser;

class User extends Common
{
    public function readByCode($code)
    {
        $model_record = AppApplicationAdminRecord::where('code', $code)->find();

        // TOOD:验证状态
        $model_user = $model_record->systemAdmin;

        return $this->returnMessage($model_user);
    }

    public function readByUid($uid)
    {
        $model_user = SystemAdmin::where('uid', $uid)->find();

        return $this->returnMessage($model_user);
    }
}
