<?php

namespace app\api\controller;

use app\admin\model\AppApplication;
use app\BaseController;
use app\model\Application;
use think\facade\Validate;
use think\validate\ValidateRule;

class Common extends BaseController
{
  public function initialize()
  {

    $post_data = $this->request->post();

    $validate = Validate::rule('key', ValidateRule::isRequire())
      ->rule('timestamp', ValidateRule::isRequire()->isNumber())
      ->rule('sign', ValidateRule::isRequire());

    if (!$validate->check($post_data)) {
      return json_message($validate->getError());
    }

    try {
      $this->verifyData($post_data, $post_data['key']);
    } catch (\Exception $e) {
      return json_message($e->getMessage());
    }
  }

  public function returnMessage($data = [], $code = 0, $msg = '')
  {

    // TODO:加上返回的数据加密

    return json_message($data);
  }

  public function verifyData($data, $key)
  {

    $model_app = AppApplication::where('key', $key)->cache(600)->find();

    if (empty($model_app)) {
      throw new \Exception('Key 错误或不存在');
    }

    return verify_sign($data, $model_app->getData('secret'));
  }
}
