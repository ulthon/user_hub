<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AppAdminClient extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('app_admin_client')
            ->setComment('用户客户端')
            ->addColumn('create_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '创建时间', ])
            ->addColumn('update_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '更新时间', ])
            ->addColumn('delete_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '', ])
            ->addColumn('admin_id', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '用户 {relation} (table:system_admin,relationBindSelect:nickname)', ])
            ->addColumn('client_type', 'string', ['limit' => '100', 'null' => '1', 'comment' => '类型', ])
            ->addColumn('login_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '登陆时间', ])
            ->addColumn('client_id', 'string', ['limit' => '100', 'null' => '0', 'comment' => '客户端标识符', ])
            ->addColumn('status', 'integer', ['limit' => '3', 'signed' => '0', 'null' => '0', 'default' => '1', 'comment' => '状态 {radio} (1:正常,1:离线)', ])
            ->create();
    }
}
