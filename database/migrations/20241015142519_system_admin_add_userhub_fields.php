<?php

use think\migration\Migrator;
use think\migration\db\Column;

class SystemAdminAddUserhubFields extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('system_admin')
            ->addColumn('uid', 'string', ['limit' => '100', 'null' => '0', 'comment' => '', ])
            ->addColumn('nickname', 'string', ['limit' => '10', 'null' => '1', 'default' => '', 'comment' => '', ])
            ->addColumn('last_login_time', 'integer', ['limit' => '11', 'null' => '0', 'default' => '0', 'comment' => '', ])
            ->addColumn('intro', 'string', ['limit' => '100', 'null' => '1', 'default' => '', 'comment' => '', ])
            ->addColumn('sex', 'integer', ['limit' => '3', 'signed' => '0', 'null' => '0', 'default' => '0', 'comment' => '', ])
            ->addColumn('super_admin', 'integer', ['limit' => '3', 'signed' => '0', 'null' => '0', 'default' => '0', 'comment' => '超级管理员 {radio} (0:禁用,1:启用,)', ])
            ->addColumn('dev_status', 'integer', ['limit' => '3', 'signed' => '0', 'null' => '1', 'default' => '0', 'comment' => '开发者状态 {radio} (0:正常用户,1:开发者用户)', ])
            ->addIndex('uid')
            ->update();
    }
}
