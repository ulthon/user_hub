<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AppApplicationDomain extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('app_application_domain')
            ->setComment('')
            ->addColumn('app_id', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '应用 {relation} (table:app_application,relationBindSelect:title)', ])
            ->addColumn('domain', 'string', ['limit' => '100', 'null' => '0', 'comment' => '域名', ])
            ->addIndex('app_id')
            ->create();
    }
}
