<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AppApplication extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('app_application')
            ->setComment('应用管理')
            ->addColumn('title', 'string', ['limit' => '100', 'null' => '0', 'comment' => '名称', ])
            ->addColumn('comment', 'string', ['limit' => '100', 'null' => '0', 'default' => '', 'comment' => '备注 {textarea}', ])
            ->addColumn('create_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '创建时间', ])
            ->addColumn('update_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '更新时间', ])
            ->addColumn('delete_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '', ])
            ->addColumn('key', 'string', ['limit' => '100', 'null' => '0', 'comment' => '', ])
            ->addColumn('secret', 'string', ['limit' => '100', 'null' => '0', 'comment' => '', ])
            ->addColumn('status', 'integer', ['limit' => '5', 'signed' => '0', 'null' => '0', 'default' => '1', 'comment' => '状态 {radio} (0:禁用,1:启用)', ])
            ->addIndex('key',['unique'=>true])
            ->addIndex('delete_time')
            ->create();
    }
}
