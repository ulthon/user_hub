<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AppApplicationAdminRecord extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('app_application_admin_record')
            ->setComment('应用授权的记录表')
            ->addColumn('create_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '', ])
            ->addColumn('update_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '', ])
            ->addColumn('delete_time', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'default' => '0', 'comment' => '', ])
            ->addColumn('code', 'string', ['limit' => '100', 'null' => '0', 'comment' => '记录码', ])
            ->addColumn('status', 'integer', ['limit' => '4', 'null' => '0', 'default' => '0', 'comment' => '状态 {radio} (0:发起授权,1:已授权,2:授权已使用)', ])
            ->addColumn('admin_id', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '用户 {relation} (table:system_admin,relationBindSelect:nickname)', ])
            ->addColumn('app_id', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '0', 'comment' => '应用 {relation} (table:app_application,relationBindSelect:title)', ])
            ->create();
    }
}
