<?php

use think\migration\Migrator;
use think\migration\db\Column;

class SystemLogin extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('system_login')
            ->setComment('系统登录方式')
            ->addColumn('create_time', 'integer', ['limit' => '10', 'signed' => '0', 'null' => '1', 'default' => '0', 'comment' => '创建时间', ])
            ->addColumn('update_time', 'integer', ['limit' => '10', 'signed' => '0', 'null' => '1', 'default' => '0', 'comment' => '', ])
            ->addColumn('delete_time', 'integer', ['limit' => '10', 'signed' => '0', 'null' => '1', 'default' => '0', 'comment' => '', ])
            ->addColumn('title', 'string', ['limit' => '100', 'null' => '0', 'comment' => '名称', ])
            ->addColumn('type', 'string', ['limit' => '100', 'null' => '0', 'comment' => '类型', ])
            ->addColumn('sort', 'string', ['limit' => '100', 'null' => '0', 'default' => '10', 'comment' => '排序', ])
            ->addColumn('status', 'integer', ['limit' => '11', 'null' => '0', 'default' => '1', 'comment' => '状态 {radio} (0:禁用,1:启用)', ])
            ->addColumn('is_support_temp', 'integer', ['limit' => '11', 'null' => '0', 'default' => '0', 'comment' => '支持临时创建 {radio} (0:禁用,1:启用)', ])
            ->addColumn('config', 'text', ['null' => '1', 'comment' => '配置', ])
            ->addColumn('config_copy_from_id', 'biginteger', ['limit' => '20', 'signed' => '0', 'null' => '1', 'comment' => '复制配置 {table} (table:system_login,valueField:id,fieldName:title)', ])
            ->create();
    }
}
