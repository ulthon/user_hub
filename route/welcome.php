<?php

use think\facade\Route;

Route::view('/', 'welcome', [
    'version' => time(),
    'data'    => [
        'description'        => '奥宏用户空间',
        'system_description' => '本项目是基于ulthon_admin实现的统一用户授权系统。',
    ],
    'navbar'  => [
        [
            'name'   => '首页',
            'active' => true,
            'href'   => 'http://admin.demo.ulthon.com',
            'target' => '_self',
        ],
        [
            'name'   => '文档',
            'active' => false,
            'href'   => 'http://doc.ulthon.com/home/read/ulthon_admin/home.html',
            'target' => '_blank',
        ],
        [
            'name'   => '演示',
            'active' => false,
            'href'   => 'http://admin.demo.ulthon.com/admin/',
            'target' => '_blank',
        ],
        [
            'name'   => '商业支持',
            'active' => false,
            'href'   => 'https://ulthon.com/bussiness.html',
            'target' => '_blank',
        ],
        [
            'name'   => 'QQ群',
            'active' => false,
            'href'   => 'https://jq.qq.com/?_wv=1027&k=TULvsosz',
            'target' => '_blank',
        ],
    ],
    'feature' => [
        [
            'name'        => '多应用授权',
            'description' => '后台可以添加多个系统，分别授权，设置安全域名等。',
        ],
        [
            'name'        => '开放接口',
            'description' => '对于不想使用页面授权的应用也可以直接请求接口。',
        ],
        [
            'name'        => '三方扩展',
            'description' => '支持对接更多的三方登录。',
        ],
        [
            'name'        => '资料验证',
            'description' => '支持多种资料验证方式，不满足的引导完善。',
        ],
        [
            'name'        => '组织架构',
            'description' => '支持动态独立的组织架构，支持同步钉钉、企微等OA体系。',
        ],
        [
            'name'        => '消息通知',
            'description' => '支持消息通知机制。',
        ],

    ],
    
]);
