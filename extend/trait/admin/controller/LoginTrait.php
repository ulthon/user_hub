<?php

namespace trait\admin\controller;

use app\admin\model\SystemAdmin;

trait LoginTrait
{
    public function register()
    {
        $captcha = 1;

        if ($this->request->isPost()) {
            $post = $this->request->post();
            $rule = [
                'username|用户名' => 'require',
                'password|密码' => 'require',
            ];
            $captcha == 1 && $rule['captcha|验证码'] = 'require|captcha';
            $this->validate($post, $rule);
            $admin = SystemAdmin::where(['username' => $post['username']])->find();
            if (!empty($admin)) {
                $this->error('用户已存在，请前往登录');
            }

            $admin = new SystemAdmin();
            $admin->username = $post['username'];

            $post['salt'] = mt_rand(100000, 999999);
            $admin->salt = $post['salt'];

            $admin->password = md5($post['password'] . $post['salt']);
            $admin->uid = uniqid();

            $admin->save();

            $this->success('注册成功，请前往登录');
        }

        $this->assign('captcha', $captcha);

        return $this->fetch();
    }
}
