$(function () {
    var fileInput = document.getElementById('json-file-input');
    fileInput.addEventListener('change', function (e) {
        var files = e.target.files;
        if (files.length > 0) {
            var file = files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                var jsonStr = e.target.result;
                // 解析json字符串，判断是否出错
                try {
                    var jsonData = JSON.parse(jsonStr);

                } catch (error) {
                    layer.msg('JSON 解析出错，请检查 JSON 格式是否正确。');
                    return;
                }
                $('#data').val(jsonStr);
            };
            reader.readAsText(file);
        }
    });
    $('#process-json-file').click(function () {


        fileInput.click();
    });

    ua.listen(function (data) {
        return data;
    }, function (res) {
        ua.msg.success(res.msg, function () {
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
            parent.$('[data-treetable-refresh]').trigger("click");
        });
    });
});