<?php

namespace base\common\model;

use think\Model;
use think\model\concern\Virtual;

class VirtualModelBase extends Model
{
    use Virtual;
}
