<?php

declare(strict_types=1);

namespace base\common\command\admin;

use think\App as ThinkApp;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;

class VersionBase extends Command
{
    public const VERSION = 'v2.0.109';

    public const PRODUCT_VERSION = '';

    public const LAYUI_VERSION = '2.8.17';

    public const COMMENT = [
        '增加菜单到导出和导入',
        '增加粘贴全局操作',
        '优化表单错误表现',
        '发布新版本',
    ];

    protected function configure()
    {
        // 指令配置
        $this->setName('admin:version')
            ->addOption('push-tag', null, Option::VALUE_NONE, '使用git命令生成tag并推送')
            ->setDescription('查看当前ulthon_admin的版本号');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        if (!empty(static::PRODUCT_VERSION)) {
            $output->info('当前版本号为：' . static::PRODUCT_VERSION);
        }
        $output->info('当前ulthon_admin版本号为：' . static::VERSION);
        $output->info('当前Layui版本号为：' . static::LAYUI_VERSION);
        $output->info('当前ThinkPHP版本号为：' . ThinkApp::VERSION);

        $output->writeln('当前的修改说明:');
        $output->writeln('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');

        foreach (static::COMMENT as  $comment) {
            $output->info($comment);
        }
        $output->writeln('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');

        $output->highlight('代码托管地址：https://gitee.com/ulthon/ulthon_admin');
        $output->highlight('开发文档地址：http://doc.ulthon.com/home/read/ulthon_admin/home.html');

        $is_push_tag = $input->hasOption('push-tag');

        if ($is_push_tag) {
            $output->writeln('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');

            $version = static::VERSION;
            if(!empty(static::PRODUCT_VERSION)) {
                $version = static::PRODUCT_VERSION;
            }
            $comment = implode(';', static::COMMENT);
            $output->info('生成标签：' . $version);
            $output->info('标签描述：' . $comment);
            exec("git tag -a $version -m \"$comment\"");
            $output->info('推送到远程仓库');
            exec('git push --tags');
        }
    }
}
